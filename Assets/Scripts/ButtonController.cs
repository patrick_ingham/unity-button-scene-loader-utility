﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;


/**
 * Utility Script for adding scene loaders to buttons
 * script needs attaching to an empty game object
 * NOTE that the gameobject will persist across scenes
 * 
 * for a UI Button to load a new scene;
 *	BUTTON needs to be named the same name as the scene to change to
 *	BUTTON needs to be tagged scenechanger
 */

public class ButtonController : MonoBehaviour
{

	public static GameObject instance;
	
	void OnEnable()
	{
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	void OnDisable()
	{
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}

	void Awake()
    {
		if (instance == null)
		{
			DontDestroyOnLoad(this.gameObject);
			instance = this.gameObject;
			
		}
		else Destroy(this.gameObject);
    }

	private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
	{
		addListenersToButtons();
	}

	private void addListenersToButtons()
	{
		Button[] buttons = FindObjectsOfType<Button>();
		foreach(Button btn in buttons)
		{
			btn.onClick.AddListener(delegate () {
				FindObjectOfType<ButtonController>().buttonClicked(btn.gameObject);
			});
		}
	}

	private void buttonClicked(GameObject gameObject)
	{
		if (gameObject.tag == "scenechanger")
		{
			loadNewScene(gameObject.name);
		}
	}

	private void loadNewScene(string name)
	{
		SceneManager.LoadScene(name);
	}
}
